package refer.rex.joseph.webservisetest;



import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiClient {
    @FormUrlEncoded
    @POST("api/Callbook/service_charge")
    Call<JsonObject> getAllHolidays(@Field("user_id") String user_id, @Field("location_id") String location_id);

    @GET("api/place/area/{id}")
    Call<JsonObject> getArea(@Path("id") String region_id);
}