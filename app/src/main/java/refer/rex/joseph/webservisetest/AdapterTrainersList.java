package refer.rex.joseph.webservisetest;

import android.content.Context;

import android.support.annotation.NonNull;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.ArrayList;

public class AdapterTrainersList extends RecyclerView.Adapter<AdapterTrainersList.HolderTrainers> {
    Context context;
    int trainersCount;
    ArrayList<Area> areaList = new ArrayList<>();


    public AdapterTrainersList(Context context, int trainersCount) {
        this.context = context;
        this.trainersCount = trainersCount;
    }

    public AdapterTrainersList(Context context, ArrayList<Area> areaList, int trainersLength) {
        this.context = context;
        this.trainersCount = trainersLength;
        this.areaList = areaList;


    }

    @NonNull
    @Override
    public HolderTrainers onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.holder_trainer_1, parent, false);
        return new HolderTrainers(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderTrainers holder, int i) {
        Area area = areaList.get(i);
        holder.txtView.setText(area.getArea());





    }

    @Override
    public int getItemCount() {
        return trainersCount;
    }

    public class HolderTrainers extends RecyclerView.ViewHolder {

        TextView txtView;

        public HolderTrainers(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            txtView = itemView.findViewById(R.id.txtView);
        }
    }
}
