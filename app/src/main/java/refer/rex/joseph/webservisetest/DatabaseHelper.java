package refer.rex.joseph.webservisetest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by appzoc on 7/5/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static String AREA_TABLE = "area_tb";
    public static String ID = "id";
    public static String AREA_ID = "area_id";
    public static String AREA_LOCATION = "area_location";
    public static String AREA_AREA = "area_area";
    public static String AREA_ISSELECTED = "area_isselected";





    String sql_query_area = "CREATE TABLE IF NOT EXISTS " + AREA_TABLE + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "  + AREA_ID + " TEXT, " + AREA_LOCATION + " TEXT, " +
            AREA_AREA + " TEXT, " + AREA_ISSELECTED + " TEXT)";


    public static String DB_NAME = "MY_DB";
    public static int DB_VERSION = 35;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sql_query_area);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + AREA_TABLE);
        onCreate(db);
    }

    public void deleteAllTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + AREA_TABLE);
        onCreate(db);
    }

    public ArrayList<Area> selectLocalNotification() {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Area> arealist = new ArrayList<Area>();

        Cursor mCursor = db.rawQuery("select * from " + AREA_TABLE, null);
        if (mCursor.moveToFirst()) {
            do {
                Area area = new Area();
                area.setId(mCursor.getString(mCursor.getColumnIndex(AREA_ID)));
                area.setArea(mCursor.getString(mCursor.getColumnIndex(AREA_AREA)));
                area.setLocation(mCursor.getString(mCursor.getColumnIndex(AREA_LOCATION)));

                if(mCursor.getString(mCursor.getColumnIndex(AREA_ISSELECTED)).equals("true")) {
                    area.setSelected(true);
                } else{ area.setSelected(false);  }


                arealist.add(area);
            } while (mCursor.moveToNext());
        }
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
//        db.close();
        //Collections.reverse(arealist);
        return arealist;
    }

    public void insertNotification(ArrayList<Area> arrayList) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "Insert or Replace into " + AREA_TABLE + " " +
                "(" + AREA_ID + ", " + AREA_LOCATION + "," +
                "" + AREA_AREA + "," + AREA_ISSELECTED +") " + "values" +
                "(?,?,?,?)";
        /*String sql = "Insert into " + AREA_TABLE + " " +
                "(" + AREA_ID + ", " + AREA_LOCATION + "," +
                "" + AREA_AREA + "," + AREA_ISSELECTED +") " + "values" +
                "(?,?,?,?)";*/
        SQLiteStatement insert = db.compileStatement(sql);
        db.beginTransaction();

        for (int i = 0; i < arrayList.size(); i++) {
            Area item = arrayList.get(i);
            insert.bindString(1, item.getId());
            insert.bindString(2, item.getLocation());
            insert.bindString(3, item.getArea());
            insert.bindString(4, String.valueOf(item.isSelected()));
            insert.execute();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
//        db.close();
    }

    public void deleteNotification(String area_id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(AREA_TABLE, AREA_ID +"=?", new String[]{area_id});
        onCreate(db);
    }

    public void updateStatus(String area_id,String area_loc, String area_area, Boolean area_isselected){
        SQLiteDatabase db=getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AREA_AREA,area_area);
        /*if(isCoplayer)
        {
            values.put(NOTIFICATION_HAS_COPLAYER,"true");
        }
        else
        {
            values.put(NOTIFICATION_HAS_COPLAYER,"false");
        }*/
        db.update(AREA_TABLE, values,AREA_ID + " = ? ",new String[]{ area_id });
    }
}
