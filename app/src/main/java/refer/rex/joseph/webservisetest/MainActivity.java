package refer.rex.joseph.webservisetest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView rvList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvList = (RecyclerView)findViewById(R.id.rvList);

        //get_hot_offer_list("2245","19");
        //get_hot_offer_list("2242","19");
        getArea("19");



    }


    private void get_hot_offer_list(String user_id, String loc_id) {


        Call<JsonObject> call = ServiceGenerator.createService(ApiClient.class).getAllHolidays(user_id, loc_id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call,
                                   Response<JsonObject> response) {
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void getArea(String loc_id) {


        Call<JsonObject> call = ServiceGenerator.createService(ApiClient.class).getArea( loc_id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i("success", "onResponse: ");

                try {
                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        if (jsonObject.getInt("ErrorCode") == 0)
                        {
                            ArrayList<Area> areaArrayList = new ArrayList<>();
                            JSONArray data = jsonObject.getJSONArray("Data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject regionObject = data.getJSONObject(i);
                                String id = regionObject.getString("id");
                                String area = regionObject.getString("area");
                                String location_id = regionObject.getString("location_id");
                                String location = regionObject.getString("location");

                                //Log.i("area", ": "+area+"\n");
                                Area area1 = new Area();
                                area1.setId(id);
                                area1.setArea(area);
                                area1.setLocation_id(location_id);
                                area1.setLocation(location);
                                areaArrayList.add(area1);
                            }

                            /*Set<Area> set = new HashSet<Area>();
                            set.addAll(databaseHelper.selectLocalNotification());
                            set.addAll(areaArrayList);
                            ArrayList<Area> noRepeatList = new ArrayList<>();
                            noRepeatList.addAll(set);*/

                            DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.this);
                            databaseHelper.insertNotification(areaArrayList);
                            loadRecycler();

                        } else { }
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                        MyToast.show(UserPreferenceActivity.this, "Sorry! Please try again", false);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i("onFailure", "onFailure: ");
                loadRecycler();

            }
        });
    }

    private void loadRecycler() {
        DatabaseHelper databaseHelper = new DatabaseHelper(MainActivity.this);
        ArrayList<Area> areaArrayList2 = new ArrayList<>();
        areaArrayList2 = databaseHelper.selectLocalNotification();


        rvList.setHasFixedSize(false);
        int numberOfColumns = 1;
        rvList.setLayoutManager(new GridLayoutManager(MainActivity.this, numberOfColumns));
        AdapterTrainersList adapter = new AdapterTrainersList(MainActivity.this,areaArrayList2, areaArrayList2.size());
        rvList.setHasFixedSize(false);
        rvList.setAdapter(adapter);
    }

}
